﻿Imports CoreTweet
Imports Newtonsoft.Json
Imports System.IO

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.FormBorderStyle = FormBorderStyle.None
        CHeck_config()
    End Sub

    'タイトルバーが無い状態でもグレー部分をドラッグすることで移動できるようにする
    Private MousePoint As Point

    Private Sub Form1_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown
        If (e.Button And MouseButtons.Left) = MouseButtons.Left Then
            MousePoint = New Point(e.X, e.Y)
        End If
    End Sub

    Private Sub Form1_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        If (e.Button And MouseButtons.Left) = MouseButtons.Left Then
            Me.Left += e.X - MousePoint.X
            Me.Top += e.Y - MousePoint.Y
        End If
    End Sub
    'ドラッグ処理ここまで

    'Ctrl + Enter キーが押されたらポストチェックに移る
    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        If e.KeyChar = Chr(13) Then
            PostChk()
            e.KeyChar = ""
        End If
    End Sub

    'ポストチェック
    '文字列の先頭が半角円マークだったときにはコマンドを実行
    'コマンドでなかった場合はステータスをアップデートする
    Private Sub PostChk()
        Dim text As String

        If Me.TextBox1.Text = "" Then
            Exit Sub
        End If

        text = Me.TextBox1.Text

        If Mid(text, 1, 1) = "\" Then
            Command(text)
        Else
            StatusUpdate(text)
        End If

        Me.TextBox1.Text = ""
    End Sub

    '実行するコマンド
    Private Sub Command(com As String)
        Select Case com
            Case "\hide", "\quit", "\exit"
                Application.Exit()
            Case "\small"
                Me.FormBorderStyle = FormBorderStyle.None
            Case "\big"
                Me.FormBorderStyle = FormBorderStyle.Sizable
            Case "\right"
                Me.Left = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width - Me.Width
            Case "\left"
                Me.Left = 0
            Case "\top"
                Me.Top = 0
            Case "\bottom"
                Me.Top = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height - Me.Height
            Case "\help"
                Dim helptext As String
                helptext = "\hide"
                helptext += vbCrLf + "\quit"
                helptext += vbCrLf + "\exit   - Exit application"
                helptext += vbCrLf + "\small  - Non Window border style"
                helptext += vbCrLf + "\big    - Window border style"
                helptext += vbCrLf + "\right  - Move to right edge of the screen"
                helptext += vbCrLf + "\left   - Move to left edge of the screen"
                helptext += vbCrLf + "\top    - Move to top edge of the screen"
                helptext += vbCrLf + "\bottom - Move to bottom edge of the screen"
                MsgBox(helptext)
        End Select
    End Sub

    '認証情報JSONの場所など
    Private Structure ConfDir
        Dim ConfDir As String
        Dim ConfJson As String
        Dim KeyJson As String
    End Structure

    Private Function StrDir() As ConfDir
        StrDir.ConfDir = Directory.GetCurrentDirectory()
        StrDir.ConfJson = StrDir.ConfDir + "\userkey.json"
        StrDir.KeyJson = StrDir.ConfDir + "\appkey.json"
    End Function

    '認証周りのセット
    Private Sub CHeck_config()
        If File.Exists(StrDir.KeyJson) = False Then
            MsgBox(
                "Consumer Key, Consumer Secret を記録した appkey.json を実行ファイルのフォルダにセットしてください。" + vbLf +
                "{" + vbLf +
                "   ""ConsumerKey"": ""%コンシューマーキー%""," + vbLf +
                "   ""ConsumerSecret"": ""%コンシューマーシークレット%""" + vbLf +
                "}"
                )
            Application.Exit()
        End If

        If File.Exists(StrDir.ConfJson) = False Then
            Authenticate()
        End If

    End Sub

    Private Structure KeySet

        Dim ConKey As String    'ConsumerKey
        Dim ConSec As String    'ConsumerSecret
        Dim AcToken As String   'AccessToken
        Dim AcSec As String     'AccessTokenSecret

    End Structure

    Private Sub Authenticate()
        Dim PIN As String
        Dim Session As OAuth.OAuthSession
        Dim Token As KeySet
        Dim newToken As Tokens
        Dim sw As StreamWriter
        Dim sr As StreamReader
        Dim keyJson As String
        Dim AccessToken As Hashtable

        PIN = ""

        sr = New StreamReader(StrDir.KeyJson)
        keyJson = sr.ReadToEnd

        Token.ConKey = JsonConvert.DeserializeObject(keyJson)("ConsumerKey").ToString
        Token.ConSec = JsonConvert.DeserializeObject(keyJson)("ConsumerSecret").ToString

        Session = OAuth.Authorize(Token.ConKey, Token.ConSec)
        System.Diagnostics.Process.Start(Session.AuthorizeUri.AbsoluteUri)

        PIN = InputBox("ブラウザで認証した後に表示される文字列を入力してください", "PIN コード入力")

        If PIN <> "" Then
            Try
                newToken = OAuth.GetTokens(Session, PIN)

                AccessToken = New Hashtable

                AccessToken("AccessToken") = newToken.AccessToken
                AccessToken("AccessTokenSecret") = newToken.AccessTokenSecret


                sw = New StreamWriter(StrDir.ConfJson, False)
                sw.Write(JsonConvert.SerializeObject(AccessToken))
                sw.Close()
                sw = Nothing
                AccessToken = Nothing

                MsgBox("認証できたと思います。")

            Catch ex As Exception

                MsgBox(
                    "認証に失敗しました。" + vbLf +
                    "アプリケーションを再起動してください" + vbLf +
                    ex.ToString
                    )

            End Try
        End If

    End Sub

    'ツイートを送信する
    Private Sub StatusUpdate(text As String)
        Dim sr As StreamReader
        Dim JsonData As String
        Dim appToken As KeySet
        Dim updater As Tokens

        sr = New StreamReader(StrDir.ConfJson)
        JsonData = sr.ReadToEnd

        appToken.AcToken = JsonConvert.DeserializeObject(JsonData)("AccessToken").ToString
        appToken.AcSec = JsonConvert.DeserializeObject(JsonData)("AccessTokenSecret").ToString

        sr = New StreamReader(StrDir.KeyJson)
        JsonData = sr.ReadToEnd

        appToken.ConKey = JsonConvert.DeserializeObject(JsonData)("ConsumerKey").ToString
        appToken.ConSec = JsonConvert.DeserializeObject(JsonData)("ConsumerSecret").ToString

        sr = Nothing
        JsonData = Nothing

        updater = CoreTweet.Tokens.Create(
            appToken.ConKey,
            appToken.ConSec,
            appToken.AcToken,
            appToken.AcSec
            )

        Try
            updater.Statuses.Update(New With {.status = text})
        Catch ex As Exception

            MsgBox("ツイートの送信に失敗しました" + vbLf +
                   ex.ToString
                   )
            Me.TextBox1.Text = ""

        End Try

    End Sub

    '入力されている文字列の長さを表示する
    Private Sub Textbox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Dim Lng As Long

        Lng = Me.TextBox1.Text.Length

        Me.Label1.Text = CStr(140 - Lng)
    End Sub
End Class
