#餃子列島
餃子列島はツイートするためだけの Twitter クライアントです。

![screenshot](./screenshot.png)

##作成環境について
Visual Studio 2015 Update 2 で作成しました。
次のライブラリに依存しています。
+ CoreTweet
+ Newtonsoft.Json

##使い方
###Visual Studio 2015 派
Git と NuGet の使える状態でコードを取ってきてソリューションファイルをダブルクリックすれば開くのでビルドできると思います。

```
git clone https://bitbucket.org/zoar/gyouzarettou/
```

###Visual Studio は使わない派
MSBuild, Git, Nuget(コマンドライン版)へのパスを通してあれば PowerShell からビルドできる*かも*しれません。

```
Invoke-WebRequest -Uri http://nkd.jp/eec4f9 -OutFile build.ps1
.\build.ps
```

`http://nkd.jp/eec4f9` は[短縮 URL サービス](http://nkd.jp/)を利用して短くした URL です。
元の URL は `https://bitbucket.org/zoar/gyouzarettou/raw/5dd497c5cfa21b6521d17bd2441379f2f2ae4624/build.ps1` です。

##注意
実行ファイルと同じフォルダに ConsumerKey と ConsumerSecret の入った JSON ファイル `appkey.json` を作成しないと動作しません。

```appkey.json
{
   "ConsumerKey": "%コンシューマーキー%",
   "ConsumerSecret": "%コンシューマーシークレット%"
}
```

キーはご自身で作成の上でご利用ください。

ライセンスは NYSL Version 0.9982 で公開します。

----
2016 K'z Minor Release - Zoar