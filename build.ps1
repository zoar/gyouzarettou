$chk = 0

if ((Get-Command git.exe 2> $null).Length -eq 0) {
   $chk = 1
   Write-Host "git.exe にアクセスできないようです"
}

if ((Get-Command msbuild.exe 2> $null).Length -eq 0) {
   $chk = 1
   Write-Host "msbuild.exe にアクセスできないようです"
}

if ((Get-Command nuget.exe 2> $null).Length -eq 0) {
   $chk = 1
   Write-Host "nuget.exe にアクセスできないようです"
}

if ($chk -eq 1) {
   exit 1
}

git.exe clone https://bitbucket.org/zoar/gyouzarettou/

Set-Location .\gyouzarettou

if ((Test-Path packages) -eq $false) {
   New-Item -Type Directory -Path ./packages > $null
}

Set-Location .\packages

nuget.exe install ..\GyouzaRettou\packages.config

Set-Location ..\

MSBuild.exe /p:Configuration=Release .\GyouzaRettou\GyouzaRettou.vbproj